# Ejercicio 3 Programacion multiproceso

## https://gitlab.com/JacobSanzJorda/tema1-ejercicio3psp.git

## Ejercicio 4
Para poder ejecutar el ejercicio 4 debes de ejecutar el jar que hay en la carpeta out\artifacts\Ejercicio4_jar\ o en el ide ejecutando la clase Ejercicio4.java, el orden de los parametros que introduzcas es indiferente.

### Ejercicio 4 main
Este metodo se encarga tanto de crear el proceso hijo y mandarle las palabras como de guardar el resultado en un fichero
### Traductor main
Este metodo se encarga de recoger la palabra y de traducirla
### Traductor rellenarHash
Este metodo se encarga de rellenar el HashMap con los datos del archivo que se le pasa como parametro
### Traductor iniciarHash
Este metodo se encarga de rellenar el HashMap con unos valores por defecto


## Ejercicio 5
Para poder ejecutar el ejercicio 4 debes de ejecutar el jar que hay en la carpeta out\artifacts\Ejercicio5_jar\ o en el ide ejecutando la clase Ejercicio5.java.

### Ejercicio 5 main
Este metodo se encarga de recoger todos los archivos y mandar uno a un proceso hijo diferente, tambien guarda los resultados recibidos en un archivo
### Adder main
Este metodo se encarga de calcular el total de los numeros que hay en el fichero