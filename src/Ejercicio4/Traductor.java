package Ejercicio4;

import java.io.*;
import java.util.HashMap;
import java.util.Scanner;

public class Traductor {

    private static HashMap<String, String> diccionario = new HashMap<String, String>();

    public static void main(String[] args) {
        //Dependiendo de si hay argumentos o no llamara a un metodo o a otro
        if (args.length <= 0) {
            iniciarHash();
        } else {
            rellenarHash(args);
        }

        //Crea el scanner y recoge la entrada
        Scanner sc = new Scanner(System.in);
        String palabra = sc.nextLine();
        String palabraTraducida;
        //Mientras la palabra no sea salir seguira en el while
        while (!palabra.equals("salir")) {
            String textoSalida;
            //Si encuentra la palabra en el hashmap entrara al if y devolvera el resultado, sino entrara al else y devolvera desconocido
            if ((palabraTraducida = diccionario.get(palabra)) != null) {
                textoSalida = palabra + " - >> " + palabraTraducida;
                System.out.println(textoSalida);
            } else {
                textoSalida = palabra + " - >> desconocido";
                System.out.println(textoSalida);
            }
            palabra = sc.nextLine();
        }
    }

    //Rellena el hashmap con los datos del archivo introducido
    private static void rellenarHash(String[] args) {
        try {
            File file = new File(args[0]);
            BufferedReader br = new BufferedReader(new FileReader(file));
            String clave = "";
            String valor = "";
            String[] conjunto;
            String conjuntoJunto;

            while ((conjuntoJunto = br.readLine()) != null) {
                conjunto = conjuntoJunto.split(" ");
                clave = conjunto[0];
                valor = conjunto[1];
                diccionario.put(clave, valor);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Rellena el hashmap con unos valores por defecto
    private static void iniciarHash() {
        diccionario.put("hello", "hola");
        diccionario.put("tree", "arbol");
        diccionario.put("car", "coche");
        diccionario.put("bye", "adios");
        diccionario.put("computer", "ordenador");
        diccionario.put("mouse", "raton");
        diccionario.put("screen", "pantalla");
        diccionario.put("mountain", "montaña");
        diccionario.put("dog", "perro");
        diccionario.put("monkey", "mono");
    }
}
