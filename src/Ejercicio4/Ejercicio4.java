package Ejercicio4;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio4 {
    public static void main(String[] args) {
        String comando;
        String argumentos = "";
        File fileTraducir = null;
        boolean archivoEntrada = false;

        //Comprueba si el argumento es un diccionario o si es un fichero a traducir
        if (args.length > 0 && args.length < 3) {
            if (args[0].equals("-d")) {
                argumentos = args[1];
                archivoEntrada = true;
            }
            if (args[0].equals("-f")) {
                fileTraducir = new File(args[1]);
            }
        }

        //Comprueba que argumento es el diccionario y que argumento es el fichero a traducir
        if (args.length > 2) {
            if (args[0].equals("-d")) {
                argumentos = args[1];
                archivoEntrada = true;
            }
            if (args[0].equals("-f")) {
                fileTraducir = new File(args[1]);
            }
            if (args[2].equals("-d")) {
                argumentos = args[3];
                archivoEntrada = true;
            }
            if (args[2].equals("-f")) {
                fileTraducir = new File(args[3]);
            }
        }

        //Guarda en la variable la ruta al jar dependiendo del sistema operativo en el que se esta ejecutando
        String sisOP = System.getProperty("os.name").toLowerCase();
        if (sisOP.equals("windows 10")) {
            comando = "java -jar out\\artifacts\\traductor_jar\\traductor.jar";
        } else {
            comando = "java -jar out/artifacts/traductor_jar/traductor.jar";
        }

        //Si la variable argumentos no esta vacia la añadira al final del comando (Guarda el nombre del fichero para pasarselo al proceso hijo)
        if (!argumentos.equals("")) {
            comando += " " + argumentos;
        }

        //Separa los argumentos por espacios en una lista y se crea el proces builder con el comando
        List<String> comand = new ArrayList<>(Arrays.asList(comando.split(" ")));
        ProcessBuilder pb;

        pb = new ProcessBuilder(comand);


        //Se crea el archivo donde se guardaran los resultados
        File file = new File("Translations.txt");
        BufferedWriter bwA = null;
        try {
            bwA = new BufferedWriter(new FileWriter(file));
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Se arranca el proceso
        try {
            Process process = pb.start();
            OutputStream os = process.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);

            String palabra;
            String devuelto;

            Scanner traductorSC = new Scanner(process.getInputStream());

            //Si no se ha añadido un -f en los argumentos entrara en este if
            if (!archivoEntrada) {
                //Pedira una palabra al usuario, mientras no sea salir seguira pidiendo.
                Scanner sc = new Scanner(System.in);
                palabra = sc.nextLine();
                while (!palabra.equals("salir")) {
                    //Envia la palabra al proceso hijo
                    bw.write(palabra);
                    bw.newLine();
                    bw.flush();

                    //Recoge la salida del proceso hijo
                    devuelto = traductorSC.nextLine();
                    System.out.println(devuelto);

                    //Escribe la salida del proceso hijo en el archivo
                    bwA.write(devuelto);
                    bwA.newLine();
                    palabra = sc.nextLine();
                }
            } else {
                //Si el usuario en los argumentos ha introducido un -f entrara aqui
                //Se crea un buffererd Reader con el archivo introducido
                FileReader fr = new FileReader(fileTraducir);
                BufferedReader br = new BufferedReader(fr);

                //Mientras la palabra recogida por el archivo no sea null seguira entrando al while
                while ((palabra = br.readLine()) != null) {
                    //Envia la palabra al proceso hijo
                    bw.write(palabra);
                    bw.newLine();
                    bw.flush();

                    //Recoge la salida y la muestra por pantalla
                    devuelto = traductorSC.nextLine();
                    System.out.println(devuelto);

                    bwA.write(devuelto);
                    bwA.newLine();
                }
                //Si el archivo se queda sin lineas mandara salir al proceso hijo para terminar el subproceso
                if ((palabra = br.readLine()) == null) {
                    bw.write("salir");
                    bw.newLine();
                    bw.flush();
                }
            }
            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bwA.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
