package Ejercicio5;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Ejercicio5 {
    private static ArrayList<String> files;

    public static void main(String[] args) {
        if (args.length > 0){
            //Se añaden todos los archivos pasados por argumento a el Array List de Strings
            files = new ArrayList<>();
            for (int i = 0; i < args.length; i++){
                files.add(args[i]);
            }

            //Se crea un buffered reader para el archivo donde se guardaran los resultados
            File fileResultado = new File("Totals.txt");
            BufferedWriter bw = null;
            try {
                 bw = new BufferedWriter(new FileWriter(fileResultado));
            } catch (IOException e) {
                e.printStackTrace();
            }
            int sumaTotal = 0;
            int resultadoFicheroActual = 0;
            //Guarda en la variable la ruta al jar dependiendo del sistema operativo en el que se esta ejecutando
            String sisOP = System.getProperty("os.name").toLowerCase();
            String comando;
            //Se recore el for tantas veces como archivos se hayan introducido
            for (int i = 0; i < files.size(); i++){
                if (sisOP.equals("windows 10")) {
                    comando = "java -jar out\\artifacts\\Adder_jar\\Adder.jar";
                } else {
                    comando = "java -jar out/artifacts/Adder_jar/Adder.jar";
                }
                //Añade el nombre del archivo a la ruta del comando
                String nombreArchivo = files.get(i);;
                comando = comando + " " + nombreArchivo;
                List<String> comand = new ArrayList<>(Arrays.asList(comando.split(" ")));
                ProcessBuilder pb = new ProcessBuilder(comand);

                //Inicia el proceso hijo
                try {
                    Process process = pb.start();
                    pb.redirectError(ProcessBuilder.Redirect.INHERIT);

                    //Recoje la entrada
                    Scanner adderSC = new Scanner(process.getInputStream());

                    String devuelto = adderSC.nextLine();
                    //Suma al resultado total el numero devuelto por el hijo
                    resultadoFicheroActual = Integer.parseInt(devuelto);
                    sumaTotal += resultadoFicheroActual;
                    //Guarda en una variable lo que se va a introducir en el archivo y lo guarda
                    String resultado = nombreArchivo + " " + resultadoFicheroActual + " Suma total: " + sumaTotal;
                    System.out.println(resultado);
                    bw.write(resultado);
                    bw.newLine();
                    bw.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            System.out.println("No se puede ejecutar por que no se han introducido archivos");
        }
    }
}
