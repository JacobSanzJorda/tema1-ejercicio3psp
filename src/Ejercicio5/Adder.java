package Ejercicio5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Adder {
    public static void main(String[] args) throws Exception {
        //Si tiene argumentos entrara al if, si no lanzara una excepcion
        if (args.length > 0){
            File file = new File(args[0]);
            //Crea un buffered reader con el fichero pasado por argumentos
            BufferedReader br = new BufferedReader(new FileReader(file));
            String palabra;
            int numero = 0;
            int numeroSuma = 0;
            //Suma a una variable el valor de el numero sacado del archivo para calcular el total
            while ((palabra = br.readLine()) != null){
                numero = Integer.parseInt(palabra);
                numeroSuma += numero;
            }
            //Devuelve el valor total
            System.out.println(numeroSuma);
        }else {
            throw new Exception();
        }
    }
}
